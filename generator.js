(function () {
    function gen_static_global() {
        const buffer = [];
        const gf = new GifWriter(buffer, 2, 2, {palette: [0xff0000, 0x0000ff]});
        gf.addFrame(0, 0, 2, 2, [0, 1, 1, 0]);
        return buffer.slice(0, gf.end());
    }

    function gen_color_strip() {
        const buffer = [];
        const gf = new GifWriter(buffer, 256, 256, {
            palette: [0x000000, 0xff0000],
            background: 1
        });

        const indices = [];
        for (let i = 0; i < 256; ++i) {
            indices.push(i);
        }

        for (let j = 0; j < 256; ++j) {
            const palette = [];

            for (let i = 0; i < 256; ++i) {
                palette.push(j << 16 | i << 8 | i);
            }

            gf.addFrame(0, j, 256, 1, indices, {palette: palette, disposal: 1});
        }

        return buffer.slice(0, gf.end());
    }

    const variants = [
        "iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==",
        base64js.fromByteArray(gen_static_global()),
        base64js.fromByteArray(gen_color_strip())
    ];
    const length = variants.length;

    const $result = document.getElementById('js-gif-result');

    let index = 0;

    function toggle(index) {
        $result.src = "data:image/gif;base64, " + variants[index];
    }

    toggle(0);

    document.getElementById("js-toggle").addEventListener("click", function () {
        index = (index + 1) % length;

        toggle(index);
    });
})();